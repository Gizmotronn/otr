---
layout: post
title:  Space Crash Course #1 - Mercury
date:   2019-06-18 13:32:20 +0800
description: Mercury is the closest planet to the Sun. Let's check it out!
img: mercury.png # Add image post (optional)
tags: [Blog, Space, Planets]
author: Adam Neilson # Add name author (optional)
---
Every week, I’ll upload a post on Medium to do with a particular object in space. We’ll start off with the planets of the Solar System, progressing to the moons and the sun, and then the asteroids and comets. After the Solar System, we’ll go over some of the greatest probes we’ve sent into space.

Ever since its discovery (nobody knows exactly who discovered Mercury, however the Ancient Romans knew about it), we’ve known Mercury was fast — it was named after the Roman messenger of the Gods. But I doubt that anyone back then knew exactly how fast Mercury moved. Thanks to modern technology, we now know that Mercury moves at roughly 170,000 kilometers an hour (for a comparison, the Earth moves at 107,000 kilometers an hour).

But not only is Mercury incredibly fast, it’s also incredibly hot and cold at the same time. Right now, half of Mercury is subject to a boiling 427 degrees Celsius (801 degrees Fahrenheit), while the other half is subject to freezing temperatures below -150 degrees Celsius (-300 degrees Fahrenheit). Why is this? The answer may surprise you: Mercury has no atmosphere.

Okay, I may be exaggerating a little. It isn’t the only reason. However, it is the reason why half is below -150 degrees Celsius — because of its close (57.9 million kilometers) distance to the Sun, Mercury’s atmosphere is constantly broken apart and scattered away by the solar wind. While Earth is affected by this as well, it is 3 times further away (149 million kilometers) and it also has a much greater mass, so it is able to hold onto its atmosphere. Mercury, being small and light, can’t put up much of a fight to hold onto its atmosphere when it is being blasted by the solar wind.

Okay, you may ask, so what? Why does an atmosphere matter?

Well, Earth has an atmosphere (shock! horror!). Without it, not only would we all be dead, the Earth would be -15 degrees Celsius. Water would be frozen and be found in the form of solid ice. Thanks to greenhouse gases (such as methane and carbon dioxide), the average temperature of the Earth is around 15 degrees Celsius. Apart from the poles, most areas on Earth have an average temperature above zero.

But because Mercury has no atmosphere, there is nothing to hold onto the heat. It is leeched all the way out into space. But the side of Mercury facing the sun is not only blasted by the solar wind, but by a lot of the heat as well. Since this heat is projected onto the front of Mercury constantly, the heat seems to stay there. If Mercury had an atmosphere, Mercury would have a consistent temperature.

Mercury has a weird rotational period. The earth rotates once every 24 hours and orbits the sun in 365.25 days. Mercury rotates 3 times for every 2 orbits — in other words, it would take 2 years for you to go to bed 3 times! One side of Mercury is almost tidally locked, so it gets all the heat from the Sun, which doesn’t make its way to the other side because there’s no atmosphere to trap the heat.

## If you can't stand the heat...
There’s plenty more about Mercury than just the temperature — let’s look at it’s profile
* Diameter: 4879 kilometers
* Distance from sun: 57.9 million kilometers
* Moons: 0
* Average Density: 13.56 grams/cubic centimeter
* Axial tilt: 2.11 degrees

Mercury has no moons. The Earth’s moon slows down its rotational period, making it 24.25 hours. Our moons also affects the axial tilt, which is 23.5 degrees. Mercury’s is only 2.11 degrees, meaning that Mercury would have no distinct seasons — just hot at day and cold at night.

In the first 50 years of the space age, only one probe visited Mercury — Mariner 10. It mapped over half of Mercury’s surface in 3 fly-bys — because of Mercury’s orbit, it was easier to put Mariner 10 into orbit around the sun rather than around Mercury — Mercury moved too fast! Mariner 10 provided much of the information that we now know about Mercury.

## Spectacular Sights
Mercury has one of the largest impact craters, the Caloris Basin. Astronomers estimate that it is less than 3 billion years old and formed after the Late Heavy Bombardment.

The Caloris Basin is a massive 1,500 kilometers in diameter, with a small peak in the middle. But it doesn’t stop there — the impact was so powerful that it sent shockwaves through the planet, forming an interesting landscape on the other side!

Most of Mercury is littered with craters. Earth has probably been hit with a lot of asteroids, too, however its atmosphere breaks them apart until they are too small to cause any damage. Any asteroids that reach the surface of Earth DO create impact craters, however the water that covers most of the Earth’s surface hides those impact craters. Very few impact craters are visible on Earth today.

However, on Mercury, you’re likely to see more craters than you’ll ever want to. Apart from the Caloris Basin, most craters are less than 50 kilometers across.

Mercury has no volcanoes, unlike the other terrestrial planets (Mercury, Venus, Earth & Mars are known as the terrestrial planets of the solar system). The reason for this is that Mercury’s mantle has cooled. The mantle is where super-hot magma/lava is located. Mercury’s mantle is cool because it is so small. Earth and Venus, on the other hand, are much larger and they can sustain volcanic eruptions (however, there is debate on how active Venus’s volcanoes have been in the past few million years).

### Thanks for reading!
