---
layout: post
title: My Hobbies for 2019-20
date: '2019-07-16 13:32:20 +0300'
description: >-
  I'll be talking about the hobbies and activities I'll be undertaking
  throughout the remainder of 2019, especially Star Wars-related activies.
img: redeemer_740x.jpg
tags:
  - Blog
  - Shopping
  - Hobbies
  - Star Wars
author: null
published: true
---
I'll be talking about the hobbies and activities I'll be undertaking throughout the remainder of 2019, especially Star Wars-related activies.




# Wishlist

## Sports
* Everton 2019-20 home kit

## Tech
* Smartwatch - maybe Samsung
* Samsung Tablet or new laptop
* Telescope w/ camera & controller

## ACORD
* Slapband USB

## Stationary
* Laptop skin - R2-D2
* Fisher Space Pen

## Science
* Planetary Society membership

## Star Wars
* R2-D2 Gigapet
* R2-D2 Sphero
* Littlebits Curiosity & Marvel
* [Lego SW Droid Developer Kit](https://www.amazon.com/s?k=lego+mindstorms+star+wars+droid+developer+kit&crid=3LT7MQOQ8TO6K&sprefix=star+wars+lego+mindstorms%2Caps%2C761&ref=nb_sb_ss_i_1_25)
* [Lego Star Wars Boost: Droid Commander](https://cnet.com/news/lego-star-wars-boost-droid-commander-could-be-the-coding-robot-set-youre-looking-for/)

### Lightsabers
* [Saberforge - Redeemer (Crystal)](https://saberforge.com/collections/crystal-sabers/products/crystal-redeemer?variant=12870606389328) - Weathered, Green Blade, Veteran Sound. View in [Saber Creator](http://www.saberparts.com/#!/configure?code=4.173.1.0-3.103.1.27-2.69.1.0-1.37.1.0)
* [Kyber Reveal by ThePachStore](https://www.thepachstore.com/collections/custom-sabers/products/lab1?fbclid=IwAR1bJSW90qN-mLFS_jaBOJL_rF4bcI3hf_CnUjDSnnu6frkkwOo6HVdZ3w8)
* [Darksaber by ThePachStore](https://www.thepachstore.com/collections/custom-sabers/products/shadowblade?variant=43208488070)

#### Ultrasabers
* [Initiate LE v5](https://ultrasabers.com/product/initiate-le-v5/)
* [Azure Reaper](https://ultrasabers.com/product/azure-reaper/)
* [The Spectre](https://ultrasabers.com/product/the-spectre/)

## Wishlist Priority
### To buy - 2019
* New lightsaber - possibly Saberfore's Crystal Redeemer or the Kyber Reveal - $400+
The lightsaber will have a green blade and crystal, reprogrammable arduino soundboard, crystal exposed, and hopefully a bladeplug. I really like the button design on Saberforge's one but it's quite expensive
* Everton 2019-20 home shirt - $75+
* Planetary Society - $50

Therefore I need at least $525 AUD. Probably more just to buy those 3 things. I want to put at least $200 in my bank, which means that I would want to earn around $800+ this year. 
I believe that I can earn this much from birthday and christmas:
* M. - $250
* E. - $200
Therefore, I could earn at least half of what I need for those holidays. Mum owes me about $50 now, and I'm looking to do more jobs around the house and on Fiverr. The lightsaber is what I'll buy around about my birthday time, and I think I can wrangle getting the shirt for my birthday. I could also maybe get something from the 2020 wishlist for birthday/christmas. It is achievable!

### To buy - 2020
* Telescope - $1k+
* R2-D2 Sphero/Lego Boost - $500
* Watch - $250
* New tablet/laptop - for Uni - $600 (for tablet) - $1.5K+
* Essendon jersey - $70
* ACORD.tech - renew domain - $40

## Wishlist secondary
### 2019
* Laptop/phone skin
### 2020
* Gigapet
* Littlebits
* Spacepen
* Slapband USB
* 3rd lightsaber? Possibly curved, blue blade....
