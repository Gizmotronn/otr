---
layout: post
title: WAAIFL 2019 Mid-Season Review
date: '2019-07-16 13:32:20 +0300'
description: >-
  The Integrated Footy season is heating up for Willetton. 3 more rounds of the
  home and away season remain before the finals. Last weeks victory over CBC
  (Green) meant that Willetton moved to an 8-2 record, with 6 consecutive wins,
  and ended Greens charge for an 8th successive finals birth. I will be talking
  about the season so far in comparison with last season, and what my goal is
  for the  remainer of the season.
img: FB_IMG_1561457813257.jpg
tags:
  - Football
  - Sports
  - Willetton
  - Hobbies
  - Australia
author: null
published: true
---
The Integrated Footy season is heating up for Willetton. 3 more rounds of the home and away season remain before the finals. Last weeks victory over CBC (Green) meant that Willetton moved to an 8-2 record, with 6 consecutive wins, and ended Greens charge for an 8th successive finals birth. I will be talking about the season so far in comparison with last season, and what my goal is for the  remainer of the season.

# Willetton Integrated - what is it?
Willetton is a proud football (that's Australian Rules Football) club formed in 1979. It's one of the closest football clubs to where I live. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/XMZYZcoAcU0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Integrated footy is a new team at the club that participates in the WAAIFL (the Western Australian Amateur Integrated Football League), which is a league for people of all ages above 14 with disabilities such as autism and AD/HD to play footy. Willetton is one of the most recent clubs to field an integrated team.

# How am I involved?
In 2017, Willetton started to become interested in fielding an Integrated side. The integrated league had been around for a few years before that. Myself, and 5 others, were part of the inaugural side. The coaches at the time were:
* Brendan Gotti
* Matthew Steacy
* Mitch Steacy
* Chan Park
* Head Coach: Keilan Marshall

I was 14 at the time and mad about footy, especially my club in the AFL - the Essendon Bombers. We found out about Willetton holding tryouts, and we jumped at the opportunity. That first year we just trained, as with only 6 players, we had nowhere near enough to play in the league - usually 18 players are on the field at any one time, with up to 4 on the bench. We played 2 scratch matches that year, one against High Wycombe, and one against CBC Green. I scored goals in both of these.

In 2018, however, we joined the league. It was a big honor to pull on the navy blue jumper, which has such a proud history here in Perth. We had about 12 players all up, so most matches we played the opposing club gave us a few of their players for the game. We won 4 matches out of 12, and finished 6th on the ladder (out of 9), which was a respectable start for us.

It took us 4 weeks to get our first win, which came against the Warnbro Swans at home. We'd played Coolbinia in our first match, who were (and still are) a really strong team. We played CBC Gold and then High Wycombe next, and I scored my first league goal in that match against High Wycombe, which incidentally happened to be our first goal against them. 

North Beach was our next win, away in the rain. We won 2 more matches for the year - both against Wembley. I kicked 9 goals in those 2 games - 6 in the first and 3 in the second. 

# The 2019 Season
## The offseason
Since I was competing in the Australian Space Design Competition during January, I missed the start of the preseason. We bought in 5 new players, 3 of them had transferred from other clubs:
* Daniel
* Tyson
* Trent (transferred from CBC Green, finished 4th in 2018)
* Richo (transferred from CBC Green, finished 4th in 2018)
* Conar (transferred from Wembley, finished 8th in 2018)
We've also bought in Brad during the midseason recruiting period, and he made his debut against CBC Green last week.

With 4 wins from 12 games last year, we needed to step up. We had some pretty handy recruits - Tyson's proven he's a gun with 15 goals in 9 games and an average of 25 disposals, while Trent & Richo were already well-established players at CBC. We started training in January, and we really ramped it up in February and March. Trent drove me and Richo from the train station to training, which was pretty good and it helped us bond a bit. 

## The first few rounds....
Our fixture:
* Kingsway (finished 1st in 2018) - W 
* High Wycombe (finished 2nd in 2018) - L
* Wembley (finished 8th in 2018) - W
* Coolbinia (finished 3rd, but won the premiership, in 2018) - L
* Warnbro (finished 7th in 2018) - W
* North Beach (finished 9th in 2018) - W
* CBC Gold (finished 5th in 2018) - W
* Coolbinia - W
* Mandurah (new team) - W
* CBC Green (finished 4th in 2018) - W
* Kingsway - TBC
* High Wycombe - TBC
* North Beach - TBC

We had a pretty hard draw. We've won 8 and only lost 2, which is incredible, but I talked to Trent when the fixtures were released and he thought we were screwed. Of the 13 games, we played the top 4 teams 7 times - more than half our games were against the top 4. Even last year, we played 6 of our 12 games against the top 4 (and it was a bit unfair as well - everyone else played 13 games, while we only played 12).

Our first task was against Kingsway, who won the minor premiership last year (they finished 1st on the ladder). They were very high scoring, and they still are, but we defeated them 20-53 away from home. We held them to only 3 goals while scoring 7 ourselves, and if we had of kicked straighter it would have been a hammering.

I had said previously that to have any chance of winning we'd need to play defensively and limit the possession and chances that Kingsway had - once we had the ball, settle and do short kickes around the ground. I said to the boys before running out to pick the closest player to you and kick it to them, mark it, and hold it up. We obviously had to practice our marking pretty heavily during the weeks leading up to the game, but the gameplan was solid. It limited our scoring opportunities - of the 11 behinds we scored, most of them were in the last quarter when we broke the deadlock. At 3 quarter time, the score was 1.1.7 to 4.0 24 - we had only 4 scoring shots to 2. But it worked in the end, with Kingsway only having 22% of the possession. It was our first ever win against Kingsway and it was a great morale booster for the start of the season.

Our second game was a shocker for the team, but a good game on a personal level for me. I kicked 3 goals in a loss to High Wycombe at home. We beat Wembley and then lost to Coolbinia. I had 4 goals from 4 games at that point. That loss to Coolbinia was our last loss (at the time of writing). The team had been competitive and had held Coolbinia to their lowest score in 2 years - 5.6.36. We only lost by 16 points.

We lifted the stopper in round 5 against Warnbro, and since then we've been winning by (mostly) huge margins - 71 against Warnbro, 163 against CBC Gold, etc. I kicked 4 goals in the first 4 games, playing as a defensive midfielder or a winger. In  the 6 games since then, I've scored 20. The team's been going strong. Our lowest winning margin was against Coolbinia at Steel Blue Oval. It was a thriller all day - we were goalless in the first half, while Coolbinia were goalless in the second and third quarters. Both teams kicked 4 goals and we won by 3. Uwane Akor sealed it twice for us with snaps from the same spot in the last quarter, but it was goal for goal in that last quarter. Apart from that game, our lowest winning margin was 33 - against Kingsway. 

Of the 10 games we've played so far, half of them have been against last year's top 4. We've won 3 of those, by a combined margin of 94 points. We've lost the other 2 by a combined margin of 27. Everyone in the team has scored at least 4 goals - even the defenders - in 10 games and we've all averaged above 10 touches.

Our biggest problem has been our ruck stocks. We would have won against CBC Green by a lot more if we had of had a premier ruckman. Their ruckman would thrust his fist out and hit the ball towards his wingers, and he'd  almost always hit the target. Our midfield were much faster than theirs, but because of his skill it took us about 30-40 seconds to tackle him, force a ball-up, and then counter. That 40 seconds was critical. When the opposition didn't have a ruckman, we ran riot. Against CBC Green, for example, we kicked 169 to 6 - and their only goal came because of a dubious free kick. That's 28 goals and 5 behinds. In the second half, we slotted 22 goals - we'd basically hit the ball forward in the ruck, get a midfielder onto the ball, and kick the goal from 40-50 meters out. Our midfield had a great day - of those 28 goals, 20 of them were scored by our mids - I scored 4. We were silky smooth and could just run in after the bounce and kick the goal. I joked with another mid, Garry, that most of our running was running back from the goals to the center of the ground.

We've performed very well against the lower teams, but I think we've done exceptionally well against the good teams as well. It's all built on our run-and-carry - and I think that's why we beat Coolbinia, who are a very scrubby, non-skills-oriented team. We played horribly in the first half, but when we started running they couldn't keep up. The margin was so low because we were too focused on celebrating and gave away some stupid free kicks.

The big areas for improvement are definitely our ruckmen, our goal-scoring accuracy, and our positioning. When we force a turnover in our defensive 50, we often don't get the ball out to our midfielders because all the mids are down with the defenders. We need our forwards to push up the ground more.

The only way to improve our ruck work would be to pick up another ruck in the offseason. 

We need to be more clinical going inside 50. Rather than bombing it long from outside 50 meters, we should pass it around and try and set up set shots rather than relying on our running. I challenged the boys to not kick for goal from outside 30m this week against Kingsway unless there was no choice - i.e. you were being tackled and you had no time to pass. Hopefully you'll see something like 11.7 rather than 7.11 like in Round 1!
