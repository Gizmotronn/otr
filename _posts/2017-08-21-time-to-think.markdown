---
layout: post
title: Essendon's 4 in a row
date: '2019-07-19 13:32:20 +0800'
description: >-
  Essendon, the team in the Australian Football League that I support, had their
  fourth win in a row against Adelaide today. I want to take a look at what that
  means.
img: 688219-tlslargelandscape.jpg
tags:
  - Sport
  - Football
  - Essendon
  - AFL
author: null
published: true
---
Essendon, the team in the Australian Football League that I support, had their fourth win in a row against Adelaide today. I want to take a look at what that means.

# Essendon's Fixtures
(As of Round 18):
* Wins: 10
* Losses: 7
* Form: LWWWW
* Position: 6th

Round 1: GWS Giants - L (Interstate)
Round 2: Saint Kilda - L
Round 3: Melbourne - W
Round 4: Brisbane - W
Round 5: North Melbourne - W
Round 6: Collingwood - L
Round 7: Geelong - L
Round 8: Sydney (Interstate)
Round 9: Fremantle - W
Round 10: Richmond - L
Round 11: *Bye*
Round 12: Carlton - W
Round 13: Hawthorn - W
Round 14: West Coast - L (Interstate)
Round 15: GWS Giants - W
Round 16: Sydney - W
Round 17: North Melbourne - W
Round 18: Adelaide - W (Interstate)
Round 19: Gold Coast - Away (Interstate)
Round 20: Port Adelaide - Home 
Round 21: Western Bulldogs - Home
Round 22: Fremantle - Away (Interstate)
Round 23: Collingwood - Away

# Our unflattering start to the season
At round 11, we were 4-7, and it looked like another mediocre, miserable season for my beloved bombers. We should have beaten the Saints, we lost to the Pies (Collingwood) by 4 points in a controversial game, and Sydney cheated us. We should have been 7-4. Against Sydney we had a shot after the siren to win the game (taken by #23 David Myers). The shot missed, but by the game's rules we should have had another shot as Sydney player Dane Rampe climbed onto the goalpost to attempt to change the result of the game (more on that later).

Last season we were 2-6, then 4-7 and narrowly missed the finals on percentage (losing to Richmond in Round 22 by 8 points was the sealer for us). We won 12 games. We won 10 of our last 14, losing only to Richmond (twice, who finished 1st, once by 8 points), Hawthorn (who finished 4th, by 4 points), and Collingwood (who finished 3rd, by 16 points, after being a goal in front at the start of the final quarter). We were 12-10, but we should have been at least 15-10. If the narrow losses had of gone the other way, and we had of pulled our heads in against Freo in Round 2 (a stupid loss), we could have made top 4.

The big problem for last year was injuries - Joe Daniher, our big man up forward and reigning best and fairest winner, was out from Round 7. His replacement, Sean "Smack" McKernan tore his hamstring in Round 18. We lost Fantasia to illness for a fair amount of the season. We lost Marty Gleeson to a serious ankle injury in the preseason. He only came back a few weeks ago. We lost over 200 games of experience, but it was more than that - we lost genuine A-Graders - Daniher was an All-Australian from 2017 with over 60 goals, Gleeson was a gun defender and Fantasia had combined with Anthony "Walla" McDonald-Tipungwuti and Cale Hooker for 120 goals during the 2017 season. 

This season, it looked bad again. 2018's best and fairest Devon Smith went under the knife due to a recurring knee concern, we AGAIN lost Fantasia for a number of weeks. Here's our injuries from the first half of the season:

* Fantasia (Forward) - Illness, hamstring
* Gleeson (Degender) - Ankle - from 2018
* Daniher (Forward) - Ostitis pubis - from 2018
* Devon Smith (Midfielder) - Knee
* Josh Begley (Forward) - Knee - from 2018
* Sam Draper (Ruck) - Knee
* Sean McKernan (Forward/Ruck) - Hamstring

We had 5 or 6 players from our best 22 out during the first half of the 2019 season. We had a total of around 400 games experience missing. That was definitely a key part in the disappointing start to the season. While we have rebounded, you never want to start poorly and be forced to play catch-up.

## Two of the most upsetting losses
As a bomber fan, I've watched more than my fair share of upsetting losses - Round 14 2017 against Sydney (by 1 point after the siren), Elimination Final 2014 against North Melbourne (by 12 points), Elimination Final 2017 against Sydney, Round 16 2016 against Saint Kilda, Round 1 2015 against Sydney (I'm really starting to hate Sydney), etc. But there were 2 games this year that were definitely up there - Collingwood on ANZAC day and Sydney up at the SCG (Sydney Cricket Ground).

There were over 90,000 in the crowd on ANZAC day that saw us get beaten by just 4 points. There were so many horrible umpiring decisions, however a player who needed to stand up but didn't was Shiel. We went after Shiel hard during last year's trade period, giving GWS 2018's first round draft pick and this year's first rounder as well. His kicking efficiency was well below 50% that day, meaning a lot of his kicks led to Collingwood scoring opportunities, or meant that some of our opportunities were wasted. He's improved as the season has worn on, but I will stand by what I said then - if he had of kicked better, we would have won, regardless of the umpiring. The worst part was seeing us go up against one of the best sides in the competition, and *almost* beat them.

But worse was yet to come. We played Sydney, up in Sydney, in Round 8. The umpiring was horrible in the second half. Sydney were awarded a dubious free kick, when the ball was probably out of bounds. Jake Stringer didn't give the ball back because he didn't hear the umpire, so the umpire awarded Sydney a 50m penalty. Jakey remonstrated with the umpire and another 50m penalty was awarded, giving Sydney an easy goal. Poor discipline cost us a goal.

We were down by 5 points when David Myers marked it about 65 meters out. The siren went. The pressure was on. David kicked long and straight, but...

...

..

He missed. Game over? Yep.

But Essendon should have been awarded another kick. Why? There's no rule that states that a player can have as many shots as he wants after the siren. 

What happened was that this little dirtbad Dane Rampe climbed onto the goalpost and started shaking it to either put David off or to try and hit the ball as it was going through. There is a rule that clearly states that "if a player intentionally climbs or shakes a goal post, the player shooting for goal will have a free kick on the goal line", meaning that a goal would essentailly be scored. The umpires knew about this, and the goal umpire shouted "Get down!" to Rampe, who stayed up there. 

The rule does not state "if a player does not move after being warned, a free kick will be paid." The rule states "if a player shakes the post, a free kick will be awarded." As soon as the umpire saw Dane on the post, he should have awarded a free kick, rather than a warning. If he had of done the right thing, we would have won (unless they made Zac Clarke take it lol. He missed some shockers that night). 

The AFL CEO praised the umpire for his "practical decision". I mean, come on! Even if the rule stated that a warning would be given before the free kick was paid, Dane didn't get down after one warning, or even two. Mr Gillon McLachlan, what were you thinking? 

This was truly a dark day.


{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
